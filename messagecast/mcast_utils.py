import socket
import struct
import collections
import time
import threading
import select


class SenderSocket:
    """
    Wrapper class for socket used for sending data.


    Attributes
    -----------

    mcast_group : tuple
        Tuple consisting of multicast address (stirng) and port number (int)

    iface_ip : str
        String representing ip address. It may be an empty string if user does not specify it.

    sock : socket.socket
        It should not be accessed directly (not that you'll go to jail, just a convention)


    Methods 
    -----------

    __init__(mcast_addr: str, port: int, iface_ip):
        simple init method. It creates mcast_group tuple which hold info regarding multicast
        address and port used for sending data. It also creates an attribute iface_ip which
        is responsible for holding ip address of the inferface user wants to use to send and
        receive data.

    create_mcast_sender_socket(blocking=False):
        method responsilbe for creating the socket itself. If no arguments are specified then
        it creates nonblocking socket. If iface_ip is not an empty string then it specifies
        IP_MULTICAST_IF to that address so that an interface of that address is used to send
        data.

    send_message(message):
        Wrapper method for sock.sendto() method. It sends given message (UTF-8 encoded) to 
        multicast group specified in mcast_group.

    def close_socket():
        Wrapper class for closing the socket. It additionally prints output to the console
        with info regarding the "stage" of closing porcess

    """

    def __init__(self, mcast_addr: str, port: int, iface_ip=""):
        self.mcast_group = (mcast_addr, port)
        self.iface_ip = iface_ip

    def create_mcast_sender_socket(self, blocking=False):
        """ Method creating a socket. """

        # creating IPv4 socket (IPv6 may be added in the future if needed)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        if blocking:
            self.sock.setblocking(1)
        else:
            self.sock.setblocking(0)

        # allowing resusage of address
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        if self.iface_ip != "":
            # user specifiec ip address of the interfce he/she wants to use to send messages
            # If that won't be the case the socket will use default interface
            self.sock.setsockopt(
                socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(self.iface_ip)
            )

        # Creating and assigning a ttl value to the multicast (we don't want it to go very far)
        ttl = struct.pack("b", 2)
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

    def send_message(self, message: str):
        """ Wrapper method for socket.socket.sendto(). """
        self.sock.sendto(bytes(message, "utf-8"), self.mcast_group)

    def close_socket(self):
        """ Wrapper method for closing the socket. """
        print("Closing sending socket")
        self.sock.close()
        print("Closed")


class ReceiverSocket:
    """
    Class similar to SenderSocket. However this one is responsible for handling socket that
    will receiver the data.


    Attributes
    -----------

    mcast_group : tuple
        Tuple represenging multicast address and port pair. It describes the packets this class
        "listens to".

    iface_ip : str
        String representing ip address of the interface user wants to use to listen to incoming
        traffic.

    sock : socket.socket
        Self explanatory. This should not be access directly by convention.

    
    Methods
    -----------

    __init__(mcast_addr: str, port: int, iface_ip=""):
        Simple init method. It creates mcast_group and iface_ip attributes

    create_mcast_receiver_socket(blocking=False):
        Creates socket.socket object that will be listening to the incoming traffic. By default
        it creates a nonblocking socket. If iface_ip object attribue is not an empty string
        it assings IP_MULTICAST_IF to the interface with that address. It also creates a membership
        request so that a multicast group will be added to that specific interface.

        In case iface_ip is not specified (i.e.: it is an empty string) then it tries to add multicast
        membership to all interfaces (however on the testing machine it proved to not be working, it adds
        a membership to one, default, internet-facing interface).

        Lastly it binds created socket to the multicast group described by multicast address present in
        mcast_group tuple.

    receive_message(n_bytes: int):
        Wrapper method for socket.socket.recv(). Method's argument is an integer representing how many bytes
        should be read from the socket.

    fileno():
        retruns object's socket fileno. This method is needed for select.select() to work on the class object
        and not on the sock attribute.

    close_socket():
        Wrapper class for closing socket. Additionally it prints info to the console about "step" of closing
        process.

    """

    def __init__(self, mcast_addr: str, port: int, iface_ip=""):
        self.mcast_group = (mcast_addr, port)
        self.iface_ip = iface_ip

    def create_mcast_receiver_socket(self, blocking=False):
        """ Method creating socket. """

        # Creating UDP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        if blocking:
            self.sock.setblocking(1)
        else:
            self.sock.setblocking(0)

        # Allowing for reusage of address
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        if self.iface_ip != "":
            # Assigning multicast interface to the one with the ip of self.iface_ip
            self.sock.setsockopt(
                socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(self.iface_ip)
            )

            # creating membership request so that the interface will be added to the multicast
            # group and in consequence be able to read network traffic destined to that multicast
            # address.
            membership_request = socket.inet_aton(self.mcast_group[0]) + socket.inet_aton(
                self.iface_ip
            )

            # setting socket option to just created membership request
            self.sock.setsockopt(
                socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, membership_request
            )
        else:
            # If self.iface_ip is not specified (i.e.: it is an empty string) then user wants to
            # use the default interface. It is supposed to add all interfaces to that multicast
            # group however it proved not always working (on some machines it was adding multicast
            # group only to one, internet facing interface)
            mreq = struct.pack(
                "4sL", socket.inet_aton(self.mcast_group[0]), socket.INADDR_ANY
            )
            self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

        self.sock.bind(("", self.mcast_group[1]))
        #self.sock.bind(self.mcast_group)
    def receive_message(self, n_bytes: int):
        """ Wrapper method for sock.recv(). """
        return self.sock.recv(n_bytes)

    def fileno(self):
        """ Method needed for select.select() to be able to work on the class object itself and not 
        on the self.sock attribute. It simply return self.sock file descriptor. """

        return self.sock.fileno()

    def close_socket(self):
        """ Wrapper method for closing the socket. It just runs self.sock.close() and prints the info
        on the status of that operation. """

        print("Closing receiving socket")
        self.sock.close()
        print("Closed")


class CommunicationHandler:
    """
    Class responsible for handling communication in MessageCast chatting application. It basically
    uses two separate threads for sending and receiving. It also uses a collection.deque for thread-safe
    communication between user GUI thread and sending thread. For receiving it uses select.select() method.


    Attributes
    -----------

    sender : SenderSocket
        Object respresenting socket which is used to sending data.

    receiver : ReceiverSocket
        Object representing socket which is used for receiving data.

    send_buffer : collections.deque
        Deque for thread-safe communication of the user GUI thread with sender thread. All messages
        that are supposed to be sent will be added to this deque.

    finish_signal_read : threading.Event
        Simple Event for letting now the reading thread when to finish execution.

    finish_signal_write : threading.Event
        Simple Event for letting now the writing thread when to finish execution.

    chat_window : client_gui.ChatWindow
        Reference for the ChatWindow in the user GUI thread. It is used to obtain username and run
        methods responsible for updating chat window.

    read_thread : threading.Thread
        reference for the reading thread. It is used only to wait for it to finish before finishing
        writing thread. 

    
    Methods
    -----------

    __init__(mcast_addres, port, iface_ip):
        Regular init method. It creates most of the needed attributes.

    initiate_chat(chat_window):
        Assigns self.chat_window to the chat_window given as an attribute. It also creates the
        threads and runs them. Additionaly it assings self.read_thread to the created thread
        responsible for reading/listening.

    read_multicast(sock, finish_signal_read):
        Method responsible for handling incoming messages. It uses select.select() to check when
        the socket is ready for reading. After that it updates chat_window with new messages.
        It skips the messages that were send from user using that application (since they are also
        sent via multicast, reading socket picks them up).
        
        For now it just skips the messages with the nick the same as user's using that application. 
        Potentionally it may lead to the situation when two user won't see each other's messages if
        they have the same nick. This can be fixed with creating a unique identifier and skipping 
        messages with own identifier. Don't know if that will be necessary to implement though.

        If finish_signal_read will be set the thread will close the socket associated with reading
        and finish its execution.

    send_multicast(sock, buffer, finish_signal_write):
        Method responsible for handling sending messages. Once the user hits "Enter" or clicks on
        "Send" the method in user GUI thread appends user's message to the self.send_buffer. If that
        buffer is not empty then send_multicast will send that message with prepended username.

        If finish_signal_write will be set the thead will close the socket associated with sending
        and finish its execution.

    """

    def __init__(self,server_addres, mcast_addres, port, iface_ip=None):
        self.sender = SenderSocket(server_addres, int(port), iface_ip)
        self.sender.create_mcast_sender_socket()

        self.receiver = ReceiverSocket(mcast_addres, int(port), iface_ip)
        self.receiver.create_mcast_receiver_socket()

        self.send_buffer = collections.deque()

        self.finish_signal_read = threading.Event()
        self.finish_signal_write = threading.Event()

        self.hello=0

    def initiate_chat(self, chat_window):
        """
        Assigning ChatWindow reference, creating writing and reading thread. Then assigning
        reference for the reading thread and running both threads.
        """

        self.chat_window = chat_window

        threads = [
            threading.Thread(
                target=self.read_multicast,
                kwargs=dict(sock=self.receiver, finish_signal_read=self.finish_signal_read),
            ),
            threading.Thread(
                target=self.send_multicast,
                kwargs=dict(
                    sock=self.sender,
                    buffer=self.send_buffer,
                    finish_signal_write=self.finish_signal_write,
                ),
            ),
            threading.Thread(target=self.send_hello)
        ]
        self.read_thread = threads[0]

        for t in threads:
            t.start()

    def send_hello(self):
        """
        method respponsible for sending hello-keep-alive message
        """
        while self.hello:
            self.sender.send_message("|HELLO|")
            time.sleep(5)

    def read_multicast(self, sock: ReceiverSocket, finish_signal_read: threading.Event):
        """
        Method responsible for reading multicast messages. It is designed to execute in the
        separate thread.
        """

        while not finish_signal_read.is_set():
            inpts, outpts, excp = select.select([sock], [], [sock])
            for s in inpts:
                incoming_data = s.receive_message(2048)
                data = incoming_data.decode("utf-8").split("|||")
                if (
                    #data[0] != "QUITTING"
                    data[0] != self.chat_window.connection_page.username.text
                    and data[0] != "QUITTING"
                ):
                    self.chat_window.incoming_message(data[0], data[1])

        sock.close_socket()

    def send_multicast(
        self,
        sock: SenderSocket,
        buffer: collections.deque,
        finish_signal_write: threading.Event,
    ):
        """
        Method responsible for sending multicast messages. It is desgined to execute in the
        separate thread.
        """

        while not finish_signal_write.is_set():
            if len(buffer) > 0:
                print("Sending")
                message = buffer.pop()
                username = self.chat_window.connection_page.username.text #connection_page
                to_send = "|||".join([username, message])
                sock.send_message(to_send)

        sock.close_socket()
