# Running the project:

## Python venv (step not necessary but highly encouraged)

```bash
python3 -m venv ps_project
source ps_project/bin/activate
```

## Installing modules

```bash
sudo apt-get update
sudo apt-get install xclip
pip install -r requirements.txt
```

It is possible that something else might be needed. I didn't need those modules on Debian, but I did on Ubuntu.

```bash
pip install pygame pillow
```

Running the project should tell you if you miss something else.

## Running project

```bash
python client_gui.py
```

## Running server (using another virtual machine is strongly recommended )

```bash
python server_mcast.py
```

# Setup running project 

## First Screen

Set Server IP , Server PORT and Username to management

## Second Screen

Add new room clicking "NEW Room" , typing name before

Get info about rooms from server, typing "INFO" button

Connect to room clicking, "Connect Room" button

## Third Screen

Set Room IP addres (default 224.3.3.1) , ROOM PORT given by server on previous page

Set Username, you want to use in this room

Join to enjoy multicast messenger :)

## Fourth Screen

Chat Window