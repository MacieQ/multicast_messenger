#socket_multicast_receiver.py
import socket
import struct
import sys
import threading
from time import time

#global room list contains all running rooms
room_list=[]



#function which implement starting new room on user-defined port 
def new_room(port):

# Room members addresses list
	#member_list=[]
	#multicast_group = '224.3.3.1'
	

# In case, you can choose different multicast address.
	
	server_address = ('', port)	
	multicast_group_send=('224.3.3.1',port)
	


# Create the socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind to the server address
	sock.bind(server_address)

# Tell the operating system to add the socket to
# the multicast group on all interfaces.
# Ability to listen on multicast address 
	"""	
	group = socket.inet_aton(multicast_group)
	mreq = struct.pack('4sL', group, socket.INADDR_ANY)
	sock.setsockopt(
	socket.IPPROTO_IP,
	socket.IP_ADD_MEMBERSHIP,mreq)
	"""

#Non-blocking on recvfrom (wait 5 sec)
	sock.settimeout(5)

#Hello Timer

	counter=time()+30

# Receive/respond loop

		
	#check if room is still on list (if room is not deleted)
	while str(port) in [x[2] for x in room_list]:
		if counter < time():
			del_index=[x[2] for x in room_list].index(str(port))
			room_list.pop(del_index)
			break
		try:
			data, address = sock.recvfrom(1024)
			if "|HELLO|" in data.decode("utf-8"):
				counter=time()+10
			else:				
				print('received {} bytes from {}'.format(len(data), address))
				print(data)

				print('sending acknowledgement to', multicast_group_send)			
				#for member in member_list:
				#	sock.sendto(data, member)
				sock.sendto(data, multicast_group_send)
		except:
			pass
	# Print info on terminal (after it thread is finished)	
	#for member in member_list:
	#			sock.sendto(b"Personal room is closed", member)	
	print("Close personal room {}".format(port))




################################# MAIN ################################



if __name__=="__main__":

	 	
	
	server_address = ('', 10000)

	
	# Create the socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	# Bind to the server address
	sock.bind(server_address)

	# Tell the operating system to add the socket to
	# the multicast group on all interfaces.
	# Ability to listen on multicast 
	"""

	multicast_group = '224.3.3.1'
	group = socket.inet_aton(multicast_group)
	mreq = struct.pack('4sL', group, socket.INADDR_ANY)
	sock.setsockopt(
    	socket.IPPROTO_IP,
    	socket.IP_ADD_MEMBERSHIP,
    	mreq)
	
	"""

	# Receive/respond loop
	while True:
		# receive data from client
		data,address = sock.recvfrom(1024)
		print('received {} bytes from {}'.format(len(data), address))

    		# convert data to string    
		data = data.decode("utf-8")


		
		# Check management message
		
		#if info message, send current room list to client
		if "|||INF|||" in data:
			address=(str(address[0]),10000)
			for i in room_list:
				sock.sendto(bytes("|||"+str('|'.join(i)),"utf-8"), address)
		print('send to{}'.format(address))
		
		# if new message, create new room (name, user, port), 
		#append room to list , start new server-thread on specific free port 
		if "|||NEW|||" in data:
			tmp=data.split("|||")
			# max room numbers 10000
			for i in range(10001,20000):
				if not str(i) in [x[2] for x in room_list]:
					room_list.append((tmp[2],tmp[0],str(i)))
					x=threading.Thread(target=new_room,args=(i,))
					x.start()
					print("added new room !")
					break
		
		
				
		#for future use:
				
		#print('sending acknowledgement to', address)
		#sock.sendto(b'ack', address)
